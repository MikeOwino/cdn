var demo = new Moovie({
    selector: "#example",
    icons: {
         path: "https://raw.githubusercontent.com/BMSVieira/moovie.js/main/icons/"
    }
});
var video = demo.video;
if (Hls.isSupported()) {
  var hls = new Hls();
  hls.loadSource('https://content.jwplatform.com/manifests/vM7nH0Kl.m3u8');
  hls.attachMedia(video);
  hls.on(Hls.Events.MANIFEST_PARSED,function() { console.log("Ready to play!"); });
}